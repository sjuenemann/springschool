## Part 1: First Contact with the de.NBI Cloud

We will start slowely and get our very first virtual machine instance running in the cloud.

The starting point for this tutorial is the de.NBI Cloud profile page (https://cloud.denbi.de/portal/).

### 1.1 Create a de.NBI Cloud Account

If you do not have a de.NBI Cloud account, please register for one
via this link: https://cloud.denbi.de/register.
You can read more about the registration process in our 
de.NBI Cloud wiki: https://cloud.denbi.de/wiki/registration/.
Please make sure to to click on “continue” if this button shows up.

If you successfully registered for a de.NBI Cloud account,
you should be able to log in to the de.NBI Cloud Portal: https://cloud.denbi.de/portal/.


### 1.2 Set an SSH key in your account

1. Click on the `Profile` tab on the left and scroll down .

2. If you have no SSH key set so far, just click on generate key and save the
private key. During this workshop you will not need this file because 
you will access all VMs via the browser. However, for your future work using
SimpleVM, we highly recommend to read the de.NBI Cloud wiki regarding
SSH keys: https://cloud.denbi.de/wiki/portal/user_information/#ssh-key

 ![](figures/key.png)


2. (optional) If you have already a personal ssh key-pair, you can also upload your *public* key here instead of generating one. 

### 1.3 Join the SpringSchool Project

If you have not done yet, please click on the following two links to request access to the springschool cloud projects:

[sspringschool](https://signup.aai.lifescience-ri.eu/fed/registrar/?vo=lifescience&targetnew=https%3A%2F%2Fsignup.aai.lifescience-ri.eu%2Ffed%2Fregistrar%2F%3Fvo%3Delixir%26targetnew%3Dhttps%253A%252F%252Fsignup.aai.lifescience-ri.eu%252Ffed%252Fregistrar%252F%253Fvo%253Ddenbi%2526group%253Dsspringschool%26targetexisting%3Dhttps%253A%252F%252Fsignup.aai.lifescience-ri.eu%252Ffed%252Fregistrar%252F%253Fvo%253Ddenbi%2526group%253Dsspringschool%26targetextended%3Dhttps%253A%252F%252Fsignup.aai.lifescience-ri.eu%252Ffed%252Fregistrar%252F%253Fvo%253Ddenbi%2526group%253Dsspringschool&targetexisting=https%3A%2F%2Fsignup.aai.lifescience-ri.eu%2Ffed%2Fregistrar%2F%3Fvo%3Delixir%26targetnew%3Dhttps%253A%252F%252Fsignup.aai.lifescience-ri.eu%252Ffed%252Fregistrar%252F%253Fvo%253Ddenbi%2526group%253Dsspringschool%26targetexisting%3Dhttps%253A%252F%252Fsignup.aai.lifescience-ri.eu%252Ffed%252Fregistrar%252F%253Fvo%253Ddenbi%2526group%253Dsspringschool%26targetextended%3Dhttps%253A%252F%252Fsignup.aai.lifescience-ri.eu%252Ffed%252Fregistrar%252F%253Fvo%253Ddenbi%2526group%253Dsspringschool&targetextended=https%3A%2F%2Fsignup.aai.lifescience-ri.eu%2Ffed%2Fregistrar%2F%3Fvo%3Delixir%26targetnew%3Dhttps%253A%252F%252Fsignup.aai.lifescience-ri.eu%252Ffed%252Fregistrar%252F%253Fvo%253Ddenbi%2526group%253Dsspringschool%26targetexisting%3Dhttps%253A%252F%252Fsignup.aai.lifescience-ri.eu%252Ffed%252Fregistrar%252F%253Fvo%253Ddenbi%2526group%253Dsspringschool%26targetextended%3Dhttps%253A%252F%252Fsignup.aai.lifescience-ri.eu%252Ffed%252Fregistrar%252F%253Fvo%253Ddenbi%2526group%253Dsspringschool)

[springschool](https://signup.aai.lifescience-ri.eu/fed/registrar/?vo=lifescience&targetnew=https%3A%2F%2Fsignup.aai.lifescience-ri.eu%2Ffed%2Fregistrar%2F%3Fvo%3Delixir%26targetnew%3Dhttps%253A%252F%252Fsignup.aai.lifescience-ri.eu%252Ffed%252Fregistrar%252F%253Fvo%253Ddenbi%2526group%253Dspringschool%26targetexisting%3Dhttps%253A%252F%252Fsignup.aai.lifescience-ri.eu%252Ffed%252Fregistrar%252F%253Fvo%253Ddenbi%2526group%253Dspringschool%26targetextended%3Dhttps%253A%252F%252Fsignup.aai.lifescience-ri.eu%252Ffed%252Fregistrar%252F%253Fvo%253Ddenbi%2526group%253Dspringschool&targetexisting=https%3A%2F%2Fsignup.aai.lifescience-ri.eu%2Ffed%2Fregistrar%2F%3Fvo%3Delixir%26targetnew%3Dhttps%253A%252F%252Fsignup.aai.lifescience-ri.eu%252Ffed%252Fregistrar%252F%253Fvo%253Ddenbi%2526group%253Dspringschool%26targetexisting%3Dhttps%253A%252F%252Fsignup.aai.lifescience-ri.eu%252Ffed%252Fregistrar%252F%253Fvo%253Ddenbi%2526group%253Dspringschool%26targetextended%3Dhttps%253A%252F%252Fsignup.aai.lifescience-ri.eu%252Ffed%252Fregistrar%252F%253Fvo%253Ddenbi%2526group%253Dspringschool&targetextended=https%3A%2F%2Fsignup.aai.lifescience-ri.eu%2Ffed%2Fregistrar%2F%3Fvo%3Delixir%26targetnew%3Dhttps%253A%252F%252Fsignup.aai.lifescience-ri.eu%252Ffed%252Fregistrar%252F%253Fvo%253Ddenbi%2526group%253Dspringschool%26targetexisting%3Dhttps%253A%252F%252Fsignup.aai.lifescience-ri.eu%252Ffed%252Fregistrar%252F%253Fvo%253Ddenbi%2526group%253Dspringschool%26targetextended%3Dhttps%253A%252F%252Fsignup.aai.lifescience-ri.eu%252Ffed%252Fregistrar%252F%253Fvo%253Ddenbi%2526group%253Dspringschool)


### 1.4 Start a VM

1. Under the `Virtual Machine` entry on the left menu, select `New Instance`.
2. Select the (only available) project `sspringschool` and choose a name for your VM (e.g. your login name).
3. Select **de.NBI default** as the flavor.
4. In the image section, please select the `Snapshots` tab and select **SpringSchool2023** image.
   ![](./figures/image.png)

5. Select the Research Environment tab and choose the following environment: 
   * Guacamole
   ![](figures/guacamole.png)
   
6. Confirm the checkboxes and click on Start.

Next to [Section 2](Part2.md)
